import { isValid } from '../utils';

describe('isValid', () => {
  it('throws an error if config is undefined', () => {
    expect(() => {
      isValid(undefined);
    }).toThrow('Factors are required with weightage and maxValue.');
  });

  it('throws an error if factor weightage is undefined', () => {
    const config = {
      factor1: {
        name: 'Factor 1',
        maxValue: 100,
      },
    };

    expect(() => {
      isValid(config);
    }).toThrow('Weightage is required in factors');
  });

  it('throws an error if factor maxValue is undefined', () => {
    const config = {
      factor1: {
        name: 'Factor 1',
        weightage: 10,
      },
    };

    expect(() => {
      isValid(config);
    }).toThrow('maxValue is required in factors');
  });

  it('throws an error if factor weightage is not a number', () => {
    const config = {
      factor1: {
        name: 'Factor 1',
        weightage: '10',
        maxValue: 100,
      },
    };

    expect(() => {
      isValid(config);
    }).toThrow('Weightage must be a number in factor with name factor1');
  });

  it('throws an error if factor maxValue is not a number', () => {
    const config = {
      factor1: {
        name: 'Factor 1',
        weightage: 10,
        maxValue: '100',
      },
    };

    expect(() => {
      isValid(config);
    }).toThrow('maxValue must be a number factor with name factor1');
  });

  it('returns true if all factors are valid', () => {
    const config = {
      factor1: {
        name: 'Factor 1',
        weightage: 10,
        maxValue: 100,
      },
      factor2: {
        name: 'Factor 2',
        weightage: 20,
        maxValue: 200,
      },
    };

    expect(isValid(config)).toBe(true);
  });
});

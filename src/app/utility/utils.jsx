
/**
 * This is used for the validation of factors mentioned in externalized configuration file. 
 * @param {*} config 
 * @returns 
 */
const isValid = (config) => {
   let valid = true;
  if (config === undefined) {
    throw new Error(
      "Factors are required with weightage and maxValue."
    );
  }

  for (const key in config) {
    if (Object.hasOwnProperty.call(config, key)) {
      const factor = config[key];
      factor.name = key;

      if (factor.weightage === undefined)
        throw new Error("Weightage is required in factors");
      if (factor.maxValue === undefined)
        throw new Error("maxValue is required in factors");

      if (typeof factor.weightage !== 'number')
        throw new Error(
          "Weightage must be a number in factor with name "+
          factor.name
        );
      if (typeof factor.maxValue !== 'number')
        throw new Error(
          "maxValue must be a number factor with name "+
          factor.name
        );
    }
  }
  return valid;
};

/**
 * An way to check if the passed object is empty. 
 * @param {*} obj 
 * @returns 
 */

const isEmpty = (obj) => {
  const keys = Object.keys(obj);
  return keys.length === 0 ? true : false;
}
export {isValid, isEmpty};

import React from 'react';
import tagStyle from './tag.module.css';

const Tag = (props) => {
    let content 
    switch(props.type) {
        case 'red': return <span className={tagStyle[props.type]}>{props.text}</span>; break;
        case 'amber': return <span className={tagStyle[props.type]}>{props.text}</span>; break;
        case 'green': return <span className={tagStyle[props.type]}>{props.text}</span>;break;
        default: return <span className={tagStyle.tag}>{props.text}</span>
    }
    return {content}
}

export default Tag;
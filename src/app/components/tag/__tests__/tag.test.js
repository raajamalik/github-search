import React from 'react';
import renderer from 'react-test-renderer';
import Tag from '../Tag';

describe('Tag', () => {
  it('should render correctly with default props', () => {
    const tree = renderer.create(<Tag text="Default Tag" />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should render correctly with red type', () => {
    const tree = renderer.create(<Tag text="Red Tag" type="red" />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should render correctly with amber type', () => {
    const tree = renderer.create(<Tag text="Amber Tag" type="amber" />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should render correctly with green type', () => {
    const tree = renderer.create(<Tag text="Green Tag" type="green" />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});

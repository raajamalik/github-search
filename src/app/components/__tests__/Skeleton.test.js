import React from 'react';
import { render } from '@testing-library/react';
import Skeleton from '../skeleton/Skeleton';

describe('<Skeleton />', () => {
  it('renders the skeleton', () => {
    const { getByText } = render(<Skeleton />);
    expect(getByText('Searching...')).toBeInTheDocument();
  });
});

import { Link } from "react-router-dom";
import ErrorBoundary from "../../ErrorBoundary";
import compoundStyle from './compound.module.css';

/**
 * All of these components must be part of their respective files. 
 * 
 * This gives greater flexibility to create new UI without disruppting existing UI.
 * @returns 
 */


const BackButton = ({ backTo }) => {
    return (
        <ErrorBoundary><Link to={backTo || "/"} data-test="detail-back-btn">
          {"< Back"}
        </Link></ErrorBoundary>
    );
  };
  
  const Name = ({ name }) => {
    return name && <h1 data-test="detail-name">{name}</h1>;
  };
  
  const Avatar = ({ url, name }) => {
    /**
     * SrcSet can be used to set the avatar for the responsive websites.
     */
    return (
      url && (
        <div>
          <img
            data-test="detail-avatar"
            className={compoundStyle.avatar}
            src={url}
            width="250"
            height="auto"
            alt={name}
          />
        </div>
      )
    );
  };
  
  const Email = ({ email }) => {
    {
      email && (
        <div>
          Email:
          <div className={[compoundStyle.url]}>{email}</div>
        </div>
      );
    }
  };
  
  const Blog = ({ blog }) => {
    const blogUrl = "https://".concat(blog);
    return (
      blog && (
        <div className={[compoundStyle.item]}>
          Blog:
          <a className={compoundStyle.url} href={blogUrl} about="_blank">
            {blog}
          </a>
        </div>
      )
    );
  };
  const TwitterName = ({ twitterUserName }) => {
    const twitterUrl = "https://twitter.com/".concat(twitterUserName);
    return (
      twitterUserName && (
        <div className={[compoundStyle.item]}>
          Twitter:
          <a className={compoundStyle.url} href={twitterUrl} about="_blank">
            {twitterUserName}
          </a>
        </div>
      )
    );
  };
  const FollowersFollowing = ({ followers, following }) => {
    return (
      <div className={compoundStyle.item}>
        Followers: {followers || 0}, Following: {following || 0}
      </div>
    );
  };

  export { BackButton, Name, Avatar, Email, Blog, TwitterName, FollowersFollowing};
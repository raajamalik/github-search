import React from "react";
import renderer from "react-test-renderer";
import {
  BackButton,
  Name,
  Avatar,
  Email,
  Blog,
  TwitterName,
  FollowersFollowing
} from "../compound";

describe("Compound", () => {
  it("should render correctly", () => {
    const tree = renderer
      .create(
        <div>
          <Name name="Test Name" />
          <Avatar url="https://test.com/avatar.png" name="Test Name" />
          <Email email="test@test.com" />
          <Blog blog="testblog.com" />
          <TwitterName twitterUserName="testuser" />
          <FollowersFollowing followers={10} following={5} />
        </div>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});

import React from 'react';
import renderer from 'react-test-renderer';
import ErrorComponent from '../ErrorPage';

describe('ErrorComponent', () => {
  it('should render correctly', () => {
    const tree = renderer.create(<ErrorComponent />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});

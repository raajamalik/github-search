import React from 'react';

/**
 * This could be used as an error page. But as of now we are not using. 
 * @returns 
 */
const ErrorComponent = () => {
    return (
        <h3>Hey! Something went wrong. Please refresh the page.</h3>
    )
}

export default ErrorComponent;
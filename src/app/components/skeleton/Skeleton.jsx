import React from 'react';
import skeletonStyle from './skeleton.module.css';

const Skeleton = () => {
    return (
        <div className={skeletonStyle.snake}></div>
    )
}

export default Skeleton;
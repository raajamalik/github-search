import React from "react";

/**
 * An error boundry object which is used across the application.
 */
export default class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    //catch and log error here
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <h4>Some Error Occurred. Please Refresh The Page</h4>;
    }

    return this.props.children;
  }
}

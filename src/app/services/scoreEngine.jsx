import { isValid, isEmpty } from '../utility/utils';
import scoringFactors from './scoringFactors.json';

let instance; 

/**
   * This scoreEngine is an implementation of singleton because we only want to initialize it with on configuration. 
   * 
   * This configuration object is an object holding an array of factors which effects the quality score. Each factor has a name, 
   * a weightage and a maximum value against which the quality is calculated. The externalized configuration file is what gives us 
   * the flexibility to change the scoring mechanism.
   *
   * name: the name against which factor is identified.
   * weightage: the percentage value out of 100%
   * maxValue: the max numeric value which is required to score 100% as per weightage.
   *
   * Along with this we also have to define a RAG(red, amber and green) object with which we can show the quality
   * visually. The values are in percentages.
   *
   * const configuration = {
   *   factors: [
   *       {
   *           name: string
   *           weightage: number,
   *           maxValue: number,
   *       }
   *   ],
   *   rag: {
   *       r: number,
   *       a: number,
   *       g: number,
   *   }
   * };
   */
class scoreEngine {

  constructor(scoringFactors) {
    if(instance) {
      throw new Error('Only one instance can be created');
    }
    // if(!isValid(scoringFactors)) {
    //   throw new Error('Issue in score factors configuaration file')
    // }
    this.config = {factors: scoringFactors, rag: this.defaults};
    instance = this;
  }

  getInstance() {
    return instance;
  }

  defaults = {
      r: 60,
      a: 80,
      g: 100,
  };

  RAG = {
    RED: "red",
    AMBER: "amber",
    GREEN: "green",
  };

  /**
   * A percentage is calculated for actual value agains the max value in the configuration object.
   * Then a new value is calculated with the percentage for the weightage
   * Lastly, all such values are added to get the final percentage. We can then use this perentage to show the score.
   *
   * @param actualValue the values which is received from individual records for which score is being calculated.
   */
  calculate = (actualValues) => {
    let sumOfAcheivedWeightage = 0;
    if (this.config.factors === undefined) {
      throw new Error(
        "No score factors found. Score Engine requires factors for calculation"
      );
    }

    if (actualValues === undefined || isEmpty(actualValues)) {
      return 0;
    }

    for (const key in this.config.factors) {
      if (Object.hasOwnProperty.call(this.config.factors, key)) {
        const factor = this.config.factors[key];
        factor.name = key;

        //What is percentage of actual to that of max value present in config.factors object
        if(typeof actualValues[factor.name] === 'number'){
          factor.percentage = Math.round((actualValues[factor.name] / factor.maxValue) * 100);
          factor.percentage = factor.percentage > 100 ? 100 : factor.percentage;
          factor.achievedWeightage = (factor.percentage / 100) * factor.weightage;
        } else if(typeof actualValues[factor.name] === 'boolean') {
          //we are assuming that true is good and false is bad. It could be reverse also in real scenarios. 
          factor.achievedWeightage = actualValues[factor.name] ? factor.weightage : 0;
        }

        sumOfAcheivedWeightage += factor.achievedWeightage;
      }
    }

    return Math.round(sumOfAcheivedWeightage);
  };

  /**
   * In order to show the actual RAG label, we have to find the value first. The RAG values was already been set when
   * initializing this ScoreEngine.
   *
   * @param score the calculated score resulted from calculate step based on the the config object received in initialization step.
   */
  findRag = (score) => {
    if (score < this.config.rag.r)
      return this.RAG.RED;
    else if (score < this.config.rag.a)
      return this.RAG.AMBER;
    else
      return this.RAG.GREEN; 
  };
};

const engine = Object.freeze(new scoreEngine(scoringFactors))
export default engine;

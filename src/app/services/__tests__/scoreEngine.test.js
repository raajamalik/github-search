import scoreEngine from '../scoreEngine';

describe('scoreEngine', () => {
  const scoringFactors = {
   factor1: { name: 'factor1', weightage: 20, maxValue: 100 },
   factor2:  { name: 'factor2', weightage: 30, maxValue: 200 },
   factor3:  { name: 'factor3', weightage: 50, maxValue: 300 },
};
  const engine = scoreEngine.getInstance();

  describe('calculate', () => {
    it('should return 0 if no factors are provided', () => {
      const result = engine.calculate({});
      expect(result).toBe(0);
    });

    it.skip('should calculate the score based on the provided factors', () => {
      const actualValues = {
            factor1: 50,
            factor2: 150,
            factor3: 250,
      };
      const expectedScore =
        50 / 100 * 20 +
        150 / 200 * 30 +
        250 / 300 * 50;
      const result = engine.calculate(actualValues);
      expect(result).toBe(Math.round(expectedScore));
    });

    it.skip('should return 100 if all factors are at their maximum value', () => {
      const actualValues = {
        factor1: 100,
        factor2: 200,
        factor3: 300,
      };
      const result = engine.calculate(actualValues);
      expect(result).toBe(100);
    });

    it.skip('should handle boolean factors correctly', () => {
      const actualValues = {
        factor1: true,
        factor2: false,
        factor3: true,
      };
      const result = engine.calculate(actualValues);
      expect(result).toBe(20 + 50);
    });
  });

  describe('findRag', () => {
    it('should return red if score is less than the red threshold', () => {
      const result = engine.findRag(40);
      expect(result).toBe('red');
    });

    it('should return amber if score is less than the amber threshold', () => {
      const result = engine.findRag(60);
      expect(result).toBe('amber');
    });

    it('should return green if score is greater than or equal to the amber threshold', () => {
      const result = engine.findRag(80);
      expect(result).toBe('green');
    });
  });
});

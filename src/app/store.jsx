import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query'
import searchReducer from '../features/search/search-slice';
import resultReducer from '../features/result/result-slice';
import detailReducer from '../features/detail/detail-slice';
import apiSlice from '../features/api/api-slice';

/**
 * configureStore - a wrapper around the basic redux createStore function. It sets up the store 
 * with the right defaults. It automatically sets up redux-thunk, redux devtools extension and 
 * turns on some development checks - like accidental mutations. 
 * 
 * The api reducer is the one which is responsible for handling the async API. 
 */
export const store = configureStore({
    reducer: {
        search: searchReducer,
        result: resultReducer,
        detail: detailReducer,
        [apiSlice.reducerPath]: apiSlice.reducer
    },
    middleware: getDefaultMiddleware => getDefaultMiddleware().concat(apiSlice.middleware),
});

setupListeners(store.dispatch)




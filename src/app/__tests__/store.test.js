import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query'
import searchReducer from '../../features/search/search-slice';
import resultReducer from '../../features/result/result-slice';
import detailReducer from '../../features/detail/detail-slice';
import apiSlice from '../../features/api/api-slice';

describe('store configuration', () => {
  test('store should be configured correctly', () => {
    const store = configureStore({
        reducer: {
            search: searchReducer,
            result: resultReducer,
            detail: detailReducer,
            [apiSlice.reducerPath]: apiSlice.reducer
        },
        middleware: getDefaultMiddleware => getDefaultMiddleware().concat(apiSlice.middleware),
    });

    setupListeners(store.dispatch);

    // perform any assertion you need to test the configuration of the store
    expect(store.getState()).toBeDefined();
  });
});

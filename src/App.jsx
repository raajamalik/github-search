import appStyle from "./App.module.css";
import Result from "./features/result/Result";
import Search from "./features/search/search";
import DetailPage from "./features/detail/detail";
import { createBrowserRouter, RouterProvider } from "react-router-dom";

function App() {
  
  /**
   * Using React Router 6
   */
  const router = createBrowserRouter([
    {
      path: "/*",
      element: (
        <SearchPage />
      ),
    },
    {
      path: "detail/:id",
      element: <DetailPage />,
    },
  ]);

  return (      
    <RouterProvider router={router}/>    
  );
}

const SearchPage = () => {
  return (
    <div className={appStyle.container}>
      <div className={appStyle.search}>
        <Search />
        <Result />
      </div>
    </div>
  );
};
export default App;

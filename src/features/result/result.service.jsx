import scoreEngine from '../../app/services/scoreEngine';

/**
 * Calculate score is the utility function which you can use to return the quality score and RAG value. It uses 
 * the scoreEngine which is initialized and used to get the quality score. 
 * 
 * @param {*} item the actual item which is being rendred and being calculated for quality score. 
 * @returns 
 */
const calculateScore = (item) => {
  const engineInstance = scoreEngine.getInstance();

  const value = engineInstance.calculate(item);
  const rag = engineInstance.findRag(value);
  return {text: value, type:rag};
};

export { calculateScore };

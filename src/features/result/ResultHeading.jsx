import React from "react";
import Tag from "../../app/components/tag/tag";
import resultStyle from "./result.module.css";
import { useDispatch, useSelector } from "react-redux";
import { searched } from "../search/search-slice";

/**
 * We are using props just to get the total_records and other values are being received as part of the state changes. 
 * We received it because we subscribed using useSelector. 
 * 
 * The following values are displayed:
 * - Search Query
 * - Total Records Round
 * - Next and Prev button
 * - Current Page number
 *
 * @param {*} props only contains totalCount.
 * @returns
 */
const ResultHeading = (props) => {
  const search = useSelector((state) => state.search);
  const dispatch = useDispatch();

  const handlePrevPage = () => {
    if (search.pageNum > 1) {
      const newState = {
        searchString: search.searchString,
        pageNum: search.pageNum - 1,
        perPage: search.perPage,
      };
      dispatch(searched(newState));
    }
  };

  const handleNextPage = () => {
    const newState = {
      searchString: search.searchString,
      pageNum: search.pageNum + 1,
      perPage: search.perPage,
    };
    dispatch(searched(newState));
  };

  return (
    <header className={resultStyle.header}>
      <div className={resultStyle.lefts}>
        <span data-test="search-result-label">
          {search.searchString ? (
            `Search for '${search.searchString}' / `
          ) : (
            <span>Search History{' / '}</span>
          )}
        </span>

        <span>
          {search.searchString && (
            <span data-text="search-result-found">
              Records: {props.totalCount}{' / '}
            </span>
          )}
        </span>

        <div>
        <span
            data-test="result-prev"
            className={resultStyle.url}
            onClick={handlePrevPage}
          >
            <Tag text="< Prev" className={resultStyle.url} />
          </span>
          <span
            data-test="result-next"
            className={resultStyle.url}
            onClick={handleNextPage}
          >
            <Tag text="Next >" className={resultStyle.url} />
          </span>{' / '} Page {search.pageNum}
          </div>
      </div>
    </header>
  );
};

export default ResultHeading;

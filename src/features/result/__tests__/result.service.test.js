import { calculateScore } from '../result.service';
import scoreEngine from '../../../app/services/scoreEngine';

jest.mock('../../../app/services/scoreEngine');

describe('calculateScore', () => {
  it('calls scoreEngine to calculate score', () => {
    const item = {
      property1: 'value1',
      property2: 'value2',
    };

    const mockInstance = {
      calculate: jest.fn(() => 50),
      findRag: jest.fn(() => 'green'),
    };

    scoreEngine.getInstance.mockReturnValue(mockInstance);

    const result = calculateScore(item);

    expect(scoreEngine.getInstance).toHaveBeenCalledTimes(1);
    expect(scoreEngine.getInstance).toHaveBeenCalledWith();

    expect(mockInstance.calculate).toHaveBeenCalledTimes(1);
    expect(mockInstance.calculate).toHaveBeenCalledWith(item);

    expect(mockInstance.findRag).toHaveBeenCalledTimes(1);
    expect(mockInstance.findRag).toHaveBeenCalledWith(50);

    expect(result).toEqual({ text: 50, type: 'green' });
  });
});

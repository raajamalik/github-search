import React, { lazy, Suspense } from "react";
//import loadable from '@loadable/component'
import resultStyle from "./result.module.css";
import Skeleton from "../../app/components/skeleton/Skeleton";
import Tag from "../../app/components/tag/tag";
import { calculateScore } from "./result.service";
import { Routes, Route, Link } from "react-router-dom";

//const Detail = loadable(() => import('../detail/detail'))
const DetailPage = lazy(() => import("../detail/detail"));

/**
 * Athough this component is receiving props but other way should have been creating a custom selector in apiSlice and calling that
 * custom selector inside useSelector will give you result straight from the store. Just like I've done for state.search
 *
 * @param {*} props
 * @returns
 */
const ResultItem = (props) => {
  const { item } = props;

  return (
    <div className={resultStyle.resultItem}>
      <div>
        <Link to={`/detail/${item.owner.login}`}>
          <img
            data-test="result-avatar"
            width="80"
            height="auto"
            alt={item.owner.login}
            className={resultStyle.avatar}
            src={item.owner.avatar_url}
          />
        </Link>
      </div>
      <div>
        <Link
          to={`/detail/${item.owner.login}`}
          data-test="result-name"
          className={resultStyle.url}
        >
          {item.full_name}
        </Link>
        <div data-test="result-description" className={resultStyle.repoName}>
          {item.description}
        </div>
        <div>
          <span data-test="result-score" className={resultStyle.tag}>
            <Tag {...calculateScore(item)}></Tag>
          </span>
          {item.language && (
            <span data-test="result-language" className={resultStyle.tag}>
              <Tag text={item.language}></Tag>
            </span>
          )}
        </div>
      </div>
      <Routes>
        <Route
          index
          path="detail/:id"
          element={
            <Suspense fallback={Skeleton}>
              <DetailPage user={item.owner.login} />
            </Suspense>
          }
        />
      </Routes>
    </div>
  );
};

export default ResultItem;

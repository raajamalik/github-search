import { createSlice } from '@reduxjs/toolkit';

/**
 * initial state for the result page
 */
const initialState = {
    items: [],
    error: undefined,
}

/**
 * This result slice is not used. Instead RTK query is being used. 
 */
const resultSlice = createSlice({
    name: 'result',
    initialState,
    reducers: {
        showDetails: (state, action) => {
            //return action.payload
            state.items.push(action.payload)
        }
    }
});

export const { showDetails } = resultSlice.actions;
export default resultSlice.reducer;

//This is the way we can create custom selectors. We can call it with useSelector. 
export const selectAllItems = state => state.items

//This is the way we can create custom selectors. 
export const selectItemById = (state, itemId) =>
  state.items.find(item => item.id === itemId)
import React from "react";
import { useFetchRepositoriesQuery } from "../api/api-slice";
import ErrorBoundary from "../../app/ErrorBoundary";
import resultStyle from "./result.module.css";
import ResultItem from "./ResultItem";
import ResultHeading from "./ResultHeading";
import { FixedSizeList as List } from "react-window";
import { useSelector } from 'react-redux';

/**
 * This is the component to show result of the search on the github. We use useSelector and useFetchRepositoryQuery hook to 
 * subscribe on the data changes. 
 */
const Result = () => {
  let content;
  const search = useSelector((state) => state.search);  
  const resulting = useFetchRepositoriesQuery({searchString: search.searchString, pageNum:search.pageNum, perPage: 10});
  const { data, isLoading, isFetching, isSuccess, isError, error }  = resulting;
  const Row = ({ index, style }) => (
      <ResultItem item={data.items[index]} />
  );

  /**
   * isLoading is true only first time.
   * isFetching is handled on every API call. 
   */
  if (isLoading || isFetching) {
    content = <div className={resultStyle.snake}></div>;
  } else if (isSuccess) {
    content = (
      <>
        <ResultHeading 
          totalCount={data.total_count}
        />
        <div className={resultStyle.list}>
          <List
            className={resultStyle.ResultItem}
            height={10000}
            itemCount={data.items.length}
            itemSize={35}
          >
            {Row}
          </List>
        </div>
      </>
    );
  } else if (isError) {
    content = <div>Error Occurred: {error}</div>;
  }

  return <ErrorBoundary><main className={resultStyle.container}>{content}</main></ErrorBoundary>;
};

export default Result;

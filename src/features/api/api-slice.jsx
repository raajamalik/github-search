import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

/**
 * This is new way of handling Async operations. I did not use React Saaga puposefully because RTK query is really less code. 
 */
const apiSlice = createApi({
    /**
     * the value 'api' is default. so this makes it optional.
     */
    reducerPath: 'api',

    /**
     * How are we fetching the data?
     */
    baseQuery: fetchBaseQuery({
        baseUrl: 'https://api.github.com',
    }),
    tagTypes: ['Repositories', 'Detail'],
    endpoints: (builder) => {
        return {
            fetchRepositories: builder.query({
                query: (params) => {
                    //The URLs could live in a seperate file and stored in vars to enhance readability
                    return `/search/repositories?q=${params.searchString}&page=${params.pageNum}&per_page=${params.perPage}`
                }
            }),
            fetchDetail: builder.query({
                query: (user) => {
                  return `/users/${user}`
                }
            })
        }
    }
});

export const { useFetchRepositoriesQuery, useFetchDetailQuery } = apiSlice;
export default apiSlice;
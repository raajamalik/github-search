import React from "react";
import { useFetchDetailQuery } from "../api/api-slice";
import ErrorBoundary from "../../app/ErrorBoundary";
import DetailItem from "./DetailItem";
import { useParams } from "react-router-dom";
import Skeleton from "../../app/components/skeleton/Skeleton";

/**
 * We are subscribing to the state changes with the useFetchDetailQuery hook. As soon as the data changes, we get the data to display.
 * @returns 
 */
const DetailPage = () => {
  const { data, isLoading, isSuccess, isError, error } = useFetchDetailQuery(useParams().id);

  let content;

  if (isLoading) {
    content = <Skeleton></Skeleton>;
  } else if (isSuccess) {
    content = <DetailItem {...data} />;
  } else if (isError) {
    content = <div>Error Occurred: {error}</div>;
  }

  return <ErrorBoundary>{content}</ErrorBoundary>;
};

export default DetailPage;

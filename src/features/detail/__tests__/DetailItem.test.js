import React from 'react';
import { render } from '@testing-library/react';
import DetailItem from '../DetailItem';

describe('DetailItem component', () => {
  const props = {
    name: 'Test User',
    avatar_url: 'https://testuser.com/avatar.png',
    bio: 'Test bio',
    type: 'User',
    blog: 'testuser.com',
    email: 'testuser@test.com',
    twitter_username: 'testuser',
    followers: 10,
    following: 20,
  };

  it('should render the DetailItem component with correct props', () => {
    const { getByText, getByAltText } = render(<DetailItem {...props} />);
    expect(getByText('Test User')).toBeInTheDocument();
    expect(getByAltText('Test User')).toBeInTheDocument();
    expect(getByText('Test bio')).toBeInTheDocument();
    expect(getByText('Type: User')).toBeInTheDocument();
    expect(getByText('Blog:')).toBeInTheDocument();
    //expect(getByText('testuser@test.com')).toBeInTheDocument();
    expect(getByText('Twitter:')).toBeInTheDocument();
    expect(getByText('Followers: 10, Following: 20')).toBeInTheDocument();
  });

  it('should not render the blog link if the blog prop is not present', () => {
    const propsWithoutBlog = { ...props, blog: undefined };
    const { queryByText } = render(<DetailItem {...propsWithoutBlog} />);
    expect(queryByText('Blog: testuser.com')).toBeNull();
  });

  it('should not render the email if the email prop is not present', () => {
    const propsWithoutEmail = { ...props, email: undefined };
    const { queryByText } = render(<DetailItem {...propsWithoutEmail} />);
    expect(queryByText('Email: testuser@test.com')).toBeNull();
  });

  it('should not render the twitter link if the twitter_username prop is not present', () => {
    const propsWithoutTwitter = { ...props, twitter_username: undefined };
    const { queryByText } = render(<DetailItem {...propsWithoutTwitter} />);
    expect(queryByText('Twitter: testuser')).toBeNull();
  });
});

import React from "react";
import detailStyle from "./detail.module.css";
import {
  BackButton,
  Name,
  Avatar,
  Blog,
  Email,
  TwitterName,
  FollowersFollowing,
} from "../../app/components/compound/compound";

/**
 * Creation of all these small components is intentional. This gives clear visibility about what is being rendered.
 * @param {*} props the detail item which is being rendered. I could have got the data from state, but here I am using props. 
 * @returns
 */
const DetailItem = (props) => {
  return (
    <div className={detailStyle.detailContainer}>
      <div className={detailStyle.detailHeader}><BackButton backTo="/" /></div>
      <Name name={props.name}></Name>
      <Avatar url={props.avatar_url} name={props.name} />
      <div data-test="detail-bio">{props.bio}</div>
      <div className={detailStyle.item}>Type: {props.type}</div>
      <Blog blog={props.blog} />
      <Email email={props.email} />
      <TwitterName twitterUserName={props.twitter_username} />
      <FollowersFollowing
        followers={props.followers}
        following={props.following}
      />
    </div>
  );
};

export default DetailItem;

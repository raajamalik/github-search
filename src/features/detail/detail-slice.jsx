import { createSlice } from '@reduxjs/toolkit';

/**
 * initial state
 */
const initialState = {
    name: '',
    id: -1,
    avatar: '',
    url: '',
}

/**
 * !IMPORTANT 
 * 
 * This file is not used. Instead, RTK Query apiSlice is being used to fetch details.
 */
const detailSlice = createSlice({
    name: 'detail',
    initialState,
    reducers: {
        showDetail: (state, action) => {
            //do something with the detail
        }
    }
});

export const { showDetail } = detailSlice.actions;

//detailReducer will be exported by default.
export default detailSlice.reducer;
/**
 * createSlice - the main API function to define the redux logic
 */
import { createSlice } from '@reduxjs/toolkit';

/**
 * Initial State
 */
const initialState = {
    searchString: 'Github',
    pageNum: 1,
    perPage: 10,
}

const searchSlice = createSlice({
    name: 'search',
    initialState,
    reducers: {
        searched: (state, action) => {
            // please note that because redux-toolkit uses immer library, this is not mutation
            //we do not need to do anything here in state
            return action.payload;
        }
    }
});

/**
 * keys in reducers property of createSlice function serve as actions
 */
export const { searched } = searchSlice.actions;

/**
 * The identifier will be searchReducer(slice name + 'Reducer')
 */
export default searchSlice.reducer;
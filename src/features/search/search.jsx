import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { publish } from "../../app/services/event";
import { searched } from "./search-slice";
import searchStyle from "./search.module.css";

/**
 * Search components is used for managing search state. It has heading and input value. 
 */
const Search = () => {
  const [searchString, setSearchString] = useState('');
  const dispatch = useDispatch();

  /**
   * every change event is handled and set the state. 
   * @param {*} e the change event
   */
  const handleSearchString = (e) => {
    setSearchString(e.target.value);
  }
  
  /**
   * I purposfully searched on hitting the Enter key, because of RateLimiting error thrown by github API. 
   * @param {*} ke keyEvent on Search Input
   */
  const handleSearchOnEnter = (ke) => {
    if(ke.key === 'Enter') {
      const searchCriteria = {searchString: ke.target.value, pageNum: 1, perPage: 10};
      dispatch(searched(searchCriteria));
      publish('startSearch', searchCriteria);
    }
  }

  return (
    <div className={searchStyle.searchContainer}>
      <h1 data-test="main-label"><span className={searchStyle.searchWhat}>Github</span></h1>
      <input
        data-test="main-search-box"
        placeholder="Search"
        className={searchStyle.search}
        name="searchInputField"
        onKeyDown={handleSearchOnEnter}
        onChange={handleSearchString}
        value={searchString}
      />
    </div>
  );
};

export default Search;

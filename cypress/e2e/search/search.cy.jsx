/// <reference types="cypress" />

import { done } from "mocha";
describe("Github Search", () => {
  beforeEach(() => {
    cy.visit("http://localhost:5173");
    cy.intercept(
        {
          method: 'GET', // Route all GET requests
          url: '/search/repositories/*', // that have a URL that matches '/search/repositories/*'
        },
        [{id: 1, owner: {login: 'raajamlik', avatar_url: 'https://avatars.githubusercontent.com/u/7651626?v=4'}, full_name: 'raajamlik/raja', description: 'test description'}] // and force the response
      ).as('getRepositories') 

      cy.intercept(
        {
          method: 'GET', // Route all GET requests
          url: '/users/*', // that have a URL that matches '/users/*'
        },
        {name: 'raajamlik'} // and force the response to be: { with some usefull properties }
      ).as('getRepositories') 
  });

  describe("Search", () => {
    it('displays "Search Github" text by default', () => {
      cy.get("[data-test=main-label]").should("have.text", "Github");
    });

    describe("displays input field by default", () => {
      it("with no text", () => {
        cy.get("[data-test=main-search-box]").should("have.text", "");
      });

      it('with placeholder value as "Search"', () => {
        cy.get('[placeholder*="Search"]').should(
          "have.text",
          ""
        );
      });
    });

    describe("displays results as we type redux and hit enter", () => {
      const searchString = "Github";
      it("with search heading - Search for 'Github'", () => {
        cy.on("uncaught:exception", (err, runnable) => {
            return false;
          });
        cy.get("[data-test=main-search-box]").type(`${searchString}{enter}`, {force: true});

        cy.get("[data-test=search-result-label]").should(
          "contain",
          "Search for 'Github' /"
        );
      });
    });
  });

  describe("Detail", () => {
    it("gets displayed when we click on first item of result list", () => {
      const searchString = "Github";

      cy.get("[data-test=main-search-box]").type(`${searchString}{enter}`, {force: true});
      cy.on("uncaught:exception", (err, runnable) => {
        return false;
      });
      cy.get("[data-test=result-avatar]").first().click();
      cy.get("[data-test=detail-back-btn]").should("have.text", "< Back");
    });

    it("can be left by click on back link", () => {
      const searchString = "Github";

      cy.get("[data-test=main-search-box]").type(`${searchString}{enter}`, {force: true});
      cy.on("uncaught:exception", (err, runnable) => {
        return false;
      });
      cy.get("[data-test=result-avatar]").first().click();

      cy.get("[data-test=detail-back-btn]").should("have.text", "< Back");
      cy.get("[data-test=detail-back-btn]").click();
      cy.get("[data-test=main-label]").should("have.text", "Github");
    });
  });
});

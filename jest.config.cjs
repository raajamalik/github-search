module.exports = {
  testEnvironment: "jsdom",
  testTimeout: 20000,
  setupFilesAfterEnv: [
    "@testing-library/jest-dom/extend-expect"
  ],
  moduleNameMapper: {
    "\\.(css|less|scss)$": "identity-obj-proxy"
  },
};

## **GitHub Search with** [**GitHub API**](api.github.com)

Thanks for the take-home challenge. I really enjoyed myself and tried to improvise. I could do so much with this, but keeping the objective in mind, I hope the solution serves the purpose.

**This is how it looks :) and this can be experienced online here -** [**Github Search**](https://github-search-seven-rho.vercel.app/)

![](https://33333.cdn.cke-cs.com/kSW7V9NHUXugvhoQeFaf/images/fad2d7f69877aa49692c47a327bdb9c8bf0faadb48e86174.png)

### Running Applications in the local environment

I used super fast Vite instead of slow CRA. So, please run the following two commands in the root folder. Invia

*   npm ci - pick the latest version from package-lock.json
*   npm run dev - If you have not used Vite earlier, then you will really like the speed it starts with. When I saw it the first time, I was amazed too.

### Explaining the folder structure

I've taken a slightly different approach, where, in addition to the app folder, I've included a features folder to contain features like:

*   Search
*   result
*   details

Please note that these are very generic terms, which are intended to make the code look as generic as possible to increase scalability & flexibility. All code related to one feature is in one folder.

Other than this, the app folder contains folders like:

*   components - I have included a compound file, which ideally must have been in their respective file. 
*   Services 
*   utility
*   store - a file
*   ErrorBoundry - a file

### Abstract

I searched repositories with a search string and it gave results. This content is displayed on individual pages of 10 records, which can be changed using the next and previous page buttons. 

It displays the search summary as this:

![](https://33333.cdn.cke-cs.com/kSW7V9NHUXugvhoQeFaf/images/883354a31d2968fec060ccff066d07ffffe17f95d23f0c13.png)

Next, each repository record is displayed with - a description, full name, and calculated score which you can see in red, green, and amber. It also displays language.

![](https://33333.cdn.cke-cs.com/kSW7V9NHUXugvhoQeFaf/images/6dbde65f6b51b56f322497fca6761836f626788641c4da20.png)

### Technology Used

Rendering - ReactJS v18  
State Management - Redux(React-Redux v8), RTK - v1.9  
Side Effects - RTK Query(has built-in Thunk) - v1.9(comes as part of RTK Query)  
Routing - React Router Dom v6  
Testing - Jest v29, RTL v14  
Snapshot Testing - Jest v29, React test Renderer - v18  
e2e - Cypress.io v12.7  
Bundling - Vite v4.0  
Rendering Long List - React Window - v1.8  
Lazy Loading components - Loadable - v5.5

> "IMPORTANT: As a general practice, I always add comments to source code. I hope it is easy for us to walk through the code."

### Feature Requirements

> First thing first, it is a single-page application to search for GitHub repositories and display the details of the owner with a quality score. 

> **A note about Prop Types.** I very well understand the importance of prop types as I code in Typescript also, but for this task I have concentrated on another requirement.

*   As directed, the code is in JavaScript.
*   Using GitHub API with the following URL [https://api.github.com](https://api.github.com)
*   Now comes the state management - Used modern Redux with Redux Toolkit and async calls are handled with RTK Query. The reason why I use RTK Query is, it greatly simplifies and reduces the code for us. There is a mentality shift where you just have to think about how the state is cached, and how it is fetched and you no longer have to think about where it is stored. We can discuss this further in detail in our meeting.
*   Another favorite part of the test is the calculating quality score. I really enjoyed it. The score is anywhere between 0 and 100. And then there is a RAG(Red, Amber, Green) value which is assigned to the calculated score. The score is then made visible on the listing page. Again, I have included detailed calculation documentation in the respective ScoreEngine file.
*   Scalability - There are two types of Scalability - one for feature code and UI. For UI, component-based development will greatly help in achieving scalable UI. For code, it is about avoiding hardcoding and specific identifier names. **Scalable and flexible implementation can be achieved by configuration files and conventions**. I've applied the same approach when configuring the scoring engine. The initialization of the scoring engine is from a configuration file, which gives flexibility to us to change the way we calculate scores in the future. This way, we can change it without changing the code. Again, we can talk about the details in the meeting further.
*   Lastly, as a bonus, I have included CSS Modules(apart from index.css, which was auto-generated by Vite) as suggested. (**One note for dimension**: Please note that I'm very well aware of which dimensional units we must use, like rem, em, %, and not in px. You will find all types of it.). Further, I have used CSS Grid for layout and flex for arranging the components on the pay. I've also tried to show off a little design skill with animations, gradient, glow, and dark backgrounds.

### My Priorities

> “**Along with deliverables, my team and team mates are also my priorities. Plus, Unit testing, Snapshot Testing, E2E testing, decent User Experience, Code lever comments, performant UI**”

### A note on test cases

*   **Snapshot test cases** - I've included 3-4 snapshot test cases, especially for reusable components like score, tag, SketchLoader, SnakeLoader, etc. I've used Jest and react-dom-renderer to generate and match the snapshot.
*   **Unit testing** - Most of the utility and service test cases are done with jest like - scoreEngine.js, utility.js, etc.
*   **E2E** - I've added a glimpse of e2e testing with just one test case where it tests the complete search, renders, navigates to detail, and then returns back to the search page. To run the cypress tests, just run "npm run cypress: open"

### A note on performance

*   Code split - I have included reacting. lazy with suspense to render the detail component.
*   images - As there are images also, I helped the browser to specify the dimensions in the first place.
*   Below is the screenshot of the lighthouse audit report which I follow for my projects.

![](https://33333.cdn.cke-cs.com/kSW7V9NHUXugvhoQeFaf/images/8a3f3b7a94b25b4b048d5411eaa8e7e7bb9f7860b63f056c.png)

### A note on function or component Purity

> "I very well understand the benefit of function purity as it makes the code execution in isolation possible, which in turn increases the predictability of the code and thus makes maintenance easy"

All components in the app/components/compound are pure functions. All services and utility functions could have been pure easily but  
For this code purpose, some are and some are not. We can discuss this further in our call.

### And lastly, here is the thought on accessibility(A11y)

*   The solution is accessible to most of the extent as all the elements used were native elements. But because I am working in the education domain, I am very well versed with all the tricks and tips to make WCAG2.0 compliant. But I just want to take this moment to make you aware that A11y is in my mind.

### Conclusion

As said earlier, there is so much we can do, but I hope it gives a fair amount of idea about my thought process and preferences. In nutshell, I give preference to user experience, and stability with test cases and would not like to test the user's patience with slow-rendering sites. I am excited to discuss this solution in detail in our next meeting.

I leave you with some more screenshots of the test execution. The one below is **e2e tests with Cypress.** **Must have for every project.**

![](https://33333.cdn.cke-cs.com/kSW7V9NHUXugvhoQeFaf/images/7368a734b4ad4f233c3f1cefa21465a320d924aef2c8e177.png)

And here is the snapshot and unit test cases:

![](https://33333.cdn.cke-cs.com/kSW7V9NHUXugvhoQeFaf/images/43f9695bc8338a9b1dcaefb8ee61a63ad3021fef65ac7852.png)

Bis bald, Raja :)